cmake_minimum_required(VERSION 3.16)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

find_package(Qt6 ${QT_MIN_VERSION} CONFIG REQUIRED COMPONENTS
    Quick
)

qt_policy(SET QTP0001 NEW)

add_executable(systemsettings)
qt6_add_qml_module(systemsettings URI org.kde.systemsettings QML_FILES Main.qml)

target_sources(systemsettings PRIVATE
    main.cpp
    qml.qrc
)

target_compile_definitions(systemsettings PRIVATE -DSRC_DIR=\"${CMAKE_SOURCE_DIR}\")

qt6_add_qml_module(comps URI org.kde.comps QML_FILES OverlayDrawer.qml IconPropertiesGroup.qml)

target_link_libraries(systemsettings PRIVATE
    Qt::Qml
    Qt::Quick
    comps
)

