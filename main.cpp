#include <QGuiApplication>
#include <QQmlEngine>
#include <QQmlComponent>

int main(int argc, char *argv[])
{
    QGuiApplication application(argc, argv);

    qmlRegisterType(QUrl::fromLocalFile(QStringLiteral(SRC_DIR "/OverlayDrawer.qml")), "org.kde.comps", 2, 0, "OverlayDrawer");

    auto c = new QQmlComponent(new QQmlEngine);
    c->loadUrl(QUrl(QStringLiteral("qrc:/kcm/kcm_test/main.qml")));

    qWarning() << c->errorString();

    auto c2 = new QQmlComponent(new QQmlEngine);
    c2->loadUrl(QUrl(QStringLiteral("qrc:/qt/qml/org/kde/systemsettings/Main.qml")));

    qWarning() << c2->errorString();

    return application.exec();
}
